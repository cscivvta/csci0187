package BibliotecaApp.model;

import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class CarteTest {
    Carte c1, c2, c3;

    @BeforeEach
    public void Setup(){
         c1 = new Carte();
         c2 =null;
         c3= new Carte();
         c3.setTitlu("Ion");
        System.out.println("Before Test");
    }

    @Test
    void getTitlu_simple1() {
        assertEquals("Ion",c3.getTitlu());
        System.out.println("c3 has title Ion");
    }

    @Test
    void getTitlu_simple2() {
        try {
            c2.getTitlu();
            assert(false); }
        catch(Exception e)
        {
            System.out.println(e.toString());
            assert(true);
        }
        System.out.println("c2 doesn't have any title");

    }

    @Test
    void getTitlu_simple3() {
        assertEquals("",c1.getTitlu());
        System.out.println("c1 has emtpy title");

    }

    @Test

    void getTitlu_compus(){
        assertEquals("Ion", c3.getTitlu());
        assertEquals("", c1.getTitlu());
       // assertEquals("", c2.getTitlu());
        System.out.println("3 gets title ok");

    }

@Disabled
    @Test

    void getTitlu_testeaza_toate(){
/*    assertAll(assertEquals("", c1.getTitlu()),
            assertEquals(null, c2),
            assertEquals("Ion", c3.getTitlu())
    );
    assertAll("Carte",
            assertEquals(null, c2),
            assertEquals("Ion", c3.getTitlu())
    );*/

    }

    @Test
    void setTitlu() {
        assertEquals("", c1.getTitlu());  //verificam ca nu avem nimic
        c1.setTitlu("Poezii");
        assertNotEquals("", c1.getTitlu());
        assertEquals("Poezii", c1.getTitlu());
        System.out.println("set title ok");


    }

    @Test
    void constrCarte(){
        Carte aux= new Carte();
        assertEquals(c1.getTitlu(), aux.getTitlu());
        assertNotEquals(null, aux);
        System.out.println("constr test ok");

    }

    @AfterEach
    public void TearDown(){
        c1=null;
        c2=null;
        c3=null;
        System.out.println("After Test");
    }

    //timeout;  varianta cu adnotarea Timeout
    @Test
    //Timeout(value=100, unit = TimeUnit.MILLISECONDS)
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    //@Timeout(5)
    public void cautaDupaAutor() {
        try {
            //Thread.sleep(100);
            TimeUnit.SECONDS.sleep(1);  //il pun fortat sa doarma 1 sec
        } catch (InterruptedException e ) {
            e.printStackTrace();

        }
        assertTrue(true);
    }
}